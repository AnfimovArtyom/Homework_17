﻿#include <iostream>
#include <locale>

/*
1.Создать класс со своими данными, скрытыми при помощи private. Сделать public метод для вывода этих данных. Протестировать. 
2. Дополнить класс Vector public методом, который будет возвращать длину (модуль) вектора. Протестировать.
*/

using namespace std;

class Vector
{
private:
	short int x;
	short int y;
	short int z;

	double VectorLength = 0;

public:
	Vector(short int x, short int y, short int z) : x(x), y(y), z(z)
	{}


	void GetLengh()
	{
		cout << x << " " << y << " " << z << endl;

		VectorLength = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		cout << "Длина (модуль) вектора = " << VectorLength << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "Russian");

	Vector v(200, 300, 400);
	v.GetLengh();

	return 0;
}